# HatkeCaptureNodeJS

One Paragraph of project description goes here

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
node.js/npm
Google developer credentials
Facebook developer credentials
```

### Installing

Clone the project and run the below commands inside the project root directory.

This will install the dependencies
```
npm i
```
Please configure the below fields in .env file to login with facebook/google.
```
FACEBOOK_CLIENT_ID
FACEBOOK_CLIENT_SECRET
GOOGLE_CLIENT_ID
GOOGLE_CLIENT_SECRET
```
This will run the application (default port: 8080)
```
node app.js
```
You can access the application on:
https://localhost:8080 (Please change the port in .env if you want to run on different port)

Endpoints:
```
https://localhost:8080/login/facebook
https://localhost:8080/login/google (yet to be tested)
```
After calling the above url, you will be redirected to the sign-in page of the provider.
If the user credentials are verified, you will be redirected to the home page and you will see the user profile returned by the provider.

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [npm](https://www.npmjs.com/) - Dependency Management
* [passport-facebook](http://www.passportjs.org/docs/facebook/) - Facebook Login
* [passport-google-oauth20](http://www.passportjs.org/packages/passport-google-oauth20/) - Google Login

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

* **Harshil Patel** - *Initial work* - (https://github.com/Harshil1989)

See also the list of [contributors](https://github.com/) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details