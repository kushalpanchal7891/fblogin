require('dotenv').config();

const express = require('express');
const passport = require('passport');
const https = require('https');
const fs = require('fs');
const path = require('path');
// Create a new Express application.
const app = express();
const FacebookStrategy = require('passport-facebook').Strategy;
const GoogleStrategy = require('passport-google-oauth20').Strategy;

const certOptions = {
    key: fs.readFileSync(path.resolve('./app/config/ssl/key.pem')),
    cert: fs.readFileSync(path.resolve('./app/config/ssl/cert.pem'))
};

var apigClientFactory = require('aws-api-gateway-client').default;

config = {
    invokeUrl: 'https://826y62gpf4.execute-api.ca-central-1.amazonaws.com/Dev',
    accessKey: '', // REQUIRED
    secretKey: '', // REQUIRED
    apiKey: '',
    region: 'ca-central-1' // REQUIRED: The region where the API is deployed.
}
var apigClient = apigClientFactory.newClient(config);

// Use application-level middleware for common functionality, including
// logging, parsing, and session handling.
app.use(require('morgan')('combined'));
app.use(require('cookie-parser')());
app.use(require('body-parser').urlencoded({ extended: true }));
app.use(require('express-session')({ secret: 'keyboard cat', resave: true, saveUninitialized: true }));

// Initialize Passport and restore authentication state, if any, from the
// session.
app.use(passport.initialize());
app.use(passport.session());

// Define routes.
app.get('/', (req, res) => {
    res.send('This is home!');
});

app.get('/login', (req, res) => {
    res.send('This is login');
});

app.get('/login/facebook',
    passport.authenticate('facebook'));

passport.use(new FacebookStrategy({
    clientID: process.env['FACEBOOK_CLIENT_ID'],
    clientSecret: process.env['FACEBOOK_CLIENT_SECRET'],
    callbackURL: process.env['FACEBOOK_CALLBACK_URL']
},
    function (accessToken, refreshToken, profile, cb) {
        // In this example, the user's Facebook profile is supplied as the user
        // record.  In a production-quality application, the Facebook profile should
        // be associated with a user record in the application's database, which
        // allows for account linking and authentication with other identity
        // providers.
        var params = {
            //This is where any header, path, or querystring request params go. The key is the parameter named as defined in the API
        };
        var pathTemplate = '/signon/facebook'
        var method = 'POST';
        var additionalParams = {
            //If there are query parameters or headers that need to be sent with the request you can add them here
            headers: {
            },
            queryParams: {
            }
        };

        app.get("/login/facebook", (req, res) => {
            var body = profile;
            apigClient.invokeApi(params, pathTemplate, method, additionalParams, body)
                .then(function (result) {
                    console.log(1);
                    console.log(result.status);
                    //This is where you would put a success callback
                    res.send(JSON.stringify(result.data));
                }).catch(function (result) {
                    //This is where you would put an error callback
                    console.log(2);
                    console.log(JSON.stringify(result));
                    res.send('Request failed...');
                });
        });
        console.log(`profile: ${JSON.stringify(profile)}`);
        return cb(null, profile);
    }));

app.get('/auth/facebook/callback',
    passport.authenticate('facebook', { failureRedirect: '/login' }),
    (req, res) => {
        // TODO: save the user in the db
        let userProfile = {};
        userProfile.id = req.user._json.id;
        userProfile.name = req.user._json.name;
        // TODO: add additional fields if required before sending the response
        res.json(userProfile);
    });

app.get('/login/google',
    passport.authenticate('google', { scope: ['profile'] }));

passport.use(new GoogleStrategy({
    clientID: process.env['GOOGLE_CLIENT_ID'],
    clientSecret: process.env['GOOGLE_CLIENT_SECRET'],
    callbackURL: process.env['GOOGLE_CALLBACK_URL']
},
    function (accessToken, refreshToken, profile, cb) {
        var params = {
            //This is where any header, path, or querystring request params go. The key is the parameter named as defined in the API
        };
        var pathTemplate = '/signon/google'
        var method = 'POST';
        var additionalParams = {
            //If there are query parameters or headers that need to be sent with the request you can add them here
            headers: {
            },
            queryParams: {
            }
        };

        app.get("/login/google", (req, res) => {
            var body = profile;
            apigClient.invokeApi(params, pathTemplate, method, additionalParams, body)
                .then(function (result) {
                    console.log(1);
                    console.log(result.status);
                    //This is where you would put a success callback
                    res.send(JSON.stringify(result.data));
                }).catch(function (result) {
                    //This is where you would put an error callback
                    console.log(2);
                    console.log(JSON.stringify(result));
                    res.send('Request failed...');
                });
        });
        console.log(`profile: ${JSON.stringify(profile)}`);
        return cb(null, profile);
    }));

app.get('/auth/google/callback',
    passport.authenticate('google', { failureRedirect: '/login' }),
    (req, res) => {
        // TODO: save the user in the db
        let userProfile = {};
        userProfile.id = req.user._json.id;
        userProfile.name = req.user._json.name;
        // TODO: add additional fields if required before sending the response
        res.json(userProfile);
    });

// Configure Passport authenticated session persistence.
//
// In order to restore authentication state across HTTP requests, Passport needs
// to serialize users into and deserialize users out of the session.  In a
// production-quality application, this would typically be as simple as
// supplying the user ID when serializing, and querying the user record by ID
// from the database when deserializing.  However, due to the fact that this
// example does not have a database, the complete Facebook profile is serialized
// and deserialized.
passport.serializeUser(function (user, cb) {
    cb(null, user);
});

passport.deserializeUser(function (obj, cb) {
    cb(null, obj);
});

// TODO: handle profile endpoint if required
// app.get('/profile',
//     require('connect-ensure-login').ensureLoggedIn(),
//     (req, res) => {
//     res.render('profile', { user: req.user });
// });

https.createServer(certOptions, app).listen(process.env['PORT'] || 8080);